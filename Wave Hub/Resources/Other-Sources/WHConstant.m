//
//  WHConstant.m
//  Wave Hub
//
//  Created by Yeung Yiu Hung on 19/11/2015.
//  Copyright © 2015 Memory Leaks. All rights reserved.
//

#import "WHConstant.h"

@implementation WHConstant

//NSString *APP_STATE_LOGGED_IN  = @"APP_STATE_LOGGED_IN";
//NSString *APP_STATE_LOGGED_OUT = @"APP_STATE_LOGGED_OUT";

NSString *remoteControlPlayButtonTapped = @"play pressed";
NSString *remoteControlPauseButtonTapped = @"pause pressed";
NSString *remoteControlStopButtonTapped = @"stop pressed";
NSString *remoteControlForwardButtonTapped = @"forward pressed";
NSString *remoteControlBackwardButtonTapped = @"backward pressed";
NSString *remoteControlOtherButtonTapped = @"other pressed";

@end
