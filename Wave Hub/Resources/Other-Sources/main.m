//
//  main.m
//  Wave Hub
//
//  Created by Yeung Yiu Hung on 11/4/15
//  Copyright (c) 2015 Memory Leaks. All rights reserved.
//

@import UIKit;

#import "WHAppDelegate.h"
#import "WHApplication.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, NSStringFromClass([WHApplication class]), NSStringFromClass([WHAppDelegate class]));
    }
}
